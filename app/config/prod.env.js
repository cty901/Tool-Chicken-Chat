/*
 * @Author: hua
 * @Date: 2019-04-26 20:00:00
 * @LastEditors: hua
 * @LastEditTime: 2019-05-29 20:03:11
 */
module.exports = {
  NODE_ENV: '"production"',
  VUE_APP_CLIENT_API: '"http://212.64.83.121:501"',
  VUE_APP_CLIENT_SOCKET:'"http://212.64.83.121:501"'
}
  